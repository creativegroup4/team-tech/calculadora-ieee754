def decimal_to_base(number, base):
    if number == 0:
        return '0'
    
    digitos = "0123456789ABCDEF"
    resultado = ''
    
    while number > 0:
        print("* remainder *"*6)
        remainder = number % base
        print(remainder)
        print("* "*20)
        resultado = digitos[remainder] + resultado
        number //= base
    
    return resultado

def base_to_decimal(number, base):
    digitos = "0123456789ABCDEF"
    result = 0
    
    for i in range(len(number)):
        digit = number[-i - 1]
        value = digitos.index(digit)
        result += value * (base ** i)
    
    return result

while True:
    print("1. Decimal a octal")
    print("2. Decimal a binario")
    print("3. Decimal a hexadecimal")
    print("4. Binario a decimal")
    print("5. Binario a octal")
    print("6. Binario a hexadecimal")
    print("7. Octal a binario")
    print("8. Octal a decimal")
    print("9. Octal a hexadecimal")
    print("10. Hexadecimal a binario")
    print("11. Hexadecimal a decimal")
    print("12. Hexadecimal a octal")
    print("13. Salir")
    
    eleccion = int(input("Seleccione una opción: "))
    
    if eleccion == 1:
        numero_decimal = float(input("Ingrese el número decimal: "))
        parte_entera = int(numero_decimal)
        parte_decimal = numero_decimal - parte_entera
        integer_result = decimal_to_base(parte_entera, 8)
        resultado_decimal = ""
        
        if parte_decimal != 0:
            resultado_decimal = "."
            for _ in range(10):  # Limitar la conversión decimal a 10 dígitos
                parte_decimal *= 8
                digit = int(parte_decimal)
                resultado_decimal += str(digit)
                parte_decimal -= digit
        
        result = integer_result + resultado_decimal
        print(f"El resultado es: {result}")
    elif eleccion == 2:
        numero_decimal = float(input("Ingrese el número decimal: "))
        parte_entera = int(numero_decimal)
        parte_decimal = numero_decimal - parte_entera
        integer_result = decimal_to_base(parte_entera, 2)
        resultado_decimal = ""
        
        if parte_decimal != 0:
            resultado_decimal = "."
            for _ in range(10):  # Limitar la conversión decimal a 10 dígitos
                parte_decimal *= 2
                digit = int(parte_decimal)
                resultado_decimal += str(digit)
                parte_decimal -= digit
        
        result = integer_result + resultado_decimal
        print(f"El resultado es: {result}")
    elif eleccion == 3:
        numero_decimal = float(input("Ingrese el número decimal: "))
        parte_entera = int(numero_decimal)
        parte_decimal = numero_decimal - parte_entera
        integer_result = decimal_to_base(parte_entera, 16)
        resultado_decimal = ""
        
        if parte_decimal != 0:
            resultado_decimal = "."
            for _ in range(10):  # Limitar la conversión decimal a 10 dígitos
                parte_decimal *= 16
                digit = int(parte_decimal)
                resultado_decimal += digits[digit]
                parte_decimal -= digit
        
        result = integer_result + resultado_decimal
        print(f"El resultado es: {result}")
    elif eleccion == 4:
        binary_number = input("Ingrese el número binario: ")
        result = base_to_decimal(binary_number, 2)
        print(f"El resultado es: {result}")
    elif eleccion == 5:
        binary_number = input("Ingrese el número binario: ")
        decimal_equivalent = base_to_decimal(binary_number, 2)
        result = decimal_to_base(decimal_equivalent, 8)
        print(f"El resultado es: {result}")
    elif eleccion == 6:
        binary_number = input("Ingrese el número binario: ")
        decimal_equivalent = base_to_decimal(binary_number, 2)
        result = decimal_to_base(decimal_equivalent, 16)
        print(f"El resultado es: {result}")
    elif eleccion == 7:
        octal_number = input("Ingrese el número octal: ")
        decimal_equivalent = base_to_decimal(octal_number, 8)
        result = decimal_to_base(decimal_equivalent, 2)
        print(f"El resultado es: {result}")
    elif eleccion == 8:
        octal_number = input("Ingrese el número octal: ")
        result = base_to_decimal(octal_number, 8)
        print(f"El resultado es: {result}")
    elif eleccion == 9:
        octal_number = input("Ingrese el número octal: ")
        decimal_equivalent = base_to_decimal(octal_number, 8)
        result = decimal_to_base(decimal_equivalent, 16)
        print(f"El resultado es: {result}")
    elif eleccion == 10:
        hex_number = input("Ingrese el número hexadecimal: ")
        decimal_equivalent = base_to_decimal(hex_number, 16)
        result = decimal_to_base(decimal_equivalent, 2)
        print(f"El resultado es: {result}")
    elif eleccion == 11:
        hex_number = input("Ingrese el número hexadecimal: ")
        result = base_to_decimal(hex_number, 16)
        print(f"El resultado es: {result}")
    elif eleccion == 12:
        hex_number = input("Ingrese el número hexadecimal: ")
        decimal_equivalent = base_to_decimal(hex_number, 16)
        result = decimal_to_base(decimal_equivalent, 8)
        print(f"El resultado es: {result}")
    elif eleccion == 13:
        break
    else:
        print("Opción no válida. Por favor, seleccione una opción válida.")
