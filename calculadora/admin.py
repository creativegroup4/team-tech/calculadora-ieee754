from django.contrib import admin
from .models import BaseConverter, IEEEFloatingPointConverter, BisectionMethod, FalsePositionMethod, DerivativeSolution

@admin.register(BaseConverter)
class BaseConverterAdmin(admin.ModelAdmin):
    list_display = ('input_number', 'decimal_result', 'binary_result', 'octal_result', 'hexadecimal_result')

@admin.register(IEEEFloatingPointConverter)
class IEEEFloatingPointConverterAdmin(admin.ModelAdmin):
    list_display = ('decimal_number', 'i33_754_representation')

@admin.register(BisectionMethod)
class BisectionMethodAdmin(admin.ModelAdmin):
    list_display = ('input_value', 'result')

@admin.register(FalsePositionMethod)
class FalsePositionMethodAdmin(admin.ModelAdmin):
    list_display = ('input_value', 'result')

@admin.register(DerivativeSolution)
class DerivativeSolutionAdmin(admin.ModelAdmin):
    list_display = ('input_expression', 'result')

# Puedes seguir agregando más clases de administración aquí si tienes más modelos
