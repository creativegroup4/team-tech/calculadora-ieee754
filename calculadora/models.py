from django.db import models

class BaseConverter(models.Model):
    input_number = models.CharField(max_length=100)
    decimal_result = models.CharField(max_length=100)
    binary_result = models.CharField(max_length=100)
    octal_result = models.CharField(max_length=100)
    hexadecimal_result = models.CharField(max_length=100)

class IEEEFloatingPointConverter(models.Model):
    decimal_number = models.FloatField()
    i33_754_representation = models.CharField(max_length=100)

class BisectionMethod(models.Model):
    input_value = models.FloatField()
    result = models.FloatField()

class FalsePositionMethod(models.Model):
    input_value = models.FloatField()
    result = models.FloatField()

class DerivativeSolution(models.Model):
    input_expression = models.CharField(max_length=200)
    result = models.CharField(max_length=200)
