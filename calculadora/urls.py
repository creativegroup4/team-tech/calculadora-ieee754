from django.urls import path
from . import views

app_name = 'calculadora'  # Agrega el nombre de tu aplicación

urlpatterns = [
    path('base_converter/', views.BaseConverterView.as_view(), name='base_converter'),
    path('ieee_converter/', views.IEEEFloatingPointConverterView.as_view(), name='ieee_converter'),
    path('bisection/', views.BisectionMethodView.as_view(), name='bisection'),
    path('false_position/', views.FalsePositionMethodView.as_view(), name='false_position'),
    path('derivatives/', views.DerivativeSolutionView.as_view(), name='derivatives'),
]
