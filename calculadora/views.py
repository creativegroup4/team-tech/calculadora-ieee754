from django.shortcuts import render, redirect
from django.views import View
from .models import BaseConverter, IEEEFloatingPointConverter, BisectionMethod, FalsePositionMethod, DerivativeSolution



class HomeView(View):
    def get(self, request):
        return render(request, 'base.html')  # Renderiza la plantilla base

class BaseConverterView(View):
    def get(self, request):
        base_converter_data = BaseConverter.objects.all()
        return render(request, 'calculadora/base_converter.html', {'base_converter_data': base_converter_data})

    def post(self, request):
        input_number = request.POST.get('input_number')
        # Realiza aquí la lógica de conversión de bases y crea una instancia de BaseConverter
        # Guarda la instancia en la base de datos
        return redirect('calculadora:base_converter')  # Redirige nuevamente a la página de conversión de bases

class IEEEFloatingPointConverterView(View):
    def get(self, request):
        ieee_converter_data = IEEEFloatingPointConverter.objects.all()
        return render(request, 'calculadora/ieee_converter.html', {'ieee_converter_data': ieee_converter_data})

    def post(self, request):
        decimal_number = request.POST.get('decimal_number')
        # Realiza aquí la lógica de conversión a IEEE 754 y crea una instancia de IEEEFloatingPointConverter
        # Guarda la instancia en la base de datos
        return redirect('calculadora:ieee_converter')  # Redirige nuevamente a la página de conversión IEEE 754

class BisectionMethodView(View):
    def get(self, request):
        bisection_data = BisectionMethod.objects.all()
        return render(request, 'calculadora/bisection.html', {'bisection_data': bisection_data})

    def post(self, request):
        input_value = float(request.POST.get('input_value'))
        # Realiza aquí la lógica del método de bisección y crea una instancia de BisectionMethod
        # Guarda la instancia en la base de datos
        return redirect('calculadora:bisection')

class FalsePositionMethodView(View):
    def get(self, request):
        false_position_data = FalsePositionMethod.objects.all()
        return render(request, 'calculadora/false_position.html', {'false_position_data': false_position_data})

    def post(self, request):
        input_value = float(request.POST.get('input_value'))
        # Realiza aquí la lógica del método de regla falsa y crea una instancia de FalsePositionMethod
        # Guarda la instancia en la base de datos
        return redirect('calculadora:false_position')

class DerivativeSolutionView(View):
    def get(self, request):
        derivatives_data = DerivativeSolution.objects.all()
        return render(request, 'calculadora/derivatives.html', {'derivatives_data': derivatives_data})

    def post(self, request):
        input_expression = request.POST.get('input_expression')
        # Realiza aquí la lógica de cálculo de derivadas y crea una instancia de DerivativeSolution
        # Guarda la instancia en la base de datos
        return redirect('calculadora:derivatives')
