import struct

def float_to_ieee754(num):
    return struct.unpack('!I', struct.pack('!f', num))[0]

def ieee754_to_float(bits):
    return struct.unpack('!f', struct.pack('!I', bits))[0]

def add_ieee754(a_bits, b_bits):
    a = ieee754_to_float(a_bits)
    b = ieee754_to_float(b_bits)
    result = a + b
    return float_to_ieee754(result)

def subtract_ieee754(a_bits, b_bits):
    a = ieee754_to_float(a_bits)
    b = ieee754_to_float(b_bits)
    result = a - b
    return float_to_ieee754(result)

def main():
    a = 3.14
    b = 1.618

    a_bits = float_to_ieee754(a)
    b_bits = float_to_ieee754(b)

    sum_bits = add_ieee754(a_bits, b_bits)
    difference_bits = subtract_ieee754(a_bits, b_bits)

    print(f"A en IEEE 754: {a_bits}")
    print(f"B en IEEE 754: {b_bits}")
    print(f"A + B en IEEE 754: {sum_bits}")
    print(f"A - B en IEEE 754: {difference_bits}")

    # Convertir de IEEE 754 a punto flotante
    a_from_bits = ieee754_to_float(a_bits)
    b_from_bits = ieee754_to_float(b_bits)
    sum_from_bits = ieee754_to_float(sum_bits)
    difference_from_bits = ieee754_to_float(difference_bits)

    print(f"A desde IEEE 754: {a_from_bits}")
    print(f"B desde IEEE 754: {b_from_bits}")
    print(f"A + B desde IEEE 754: {sum_from_bits}")
    print(f"A - B desde IEEE 754: {difference_from_bits}")

if __name__ == "__main__":
    main()
