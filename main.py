import bases

# Program  calculator
"""
Octal
Binario
Hexadecimal
"""

def definicion_calculadora():
    return "Calculadora"

def main():
    print("*"*20)
    print("Calculadora elegida: ")
    print("*"*20)
    print("1. Decimal a otras bases")
    opcion = int(input("Elige una opcion (1/2): "))

    if opcion == 1:
        decimal = float(input("Ingresa el numero decimal: "))
        print(f"Binario: {decimal_a_binario(int(decimal))}")
        print(f"Octal: {decimal_a_octal(int(decimal))}")
        print(f"Hexadecimal: {decimal_a_hexadecimal(int(decimal))}")
    elif opcion == 2:
        print("")
    else:
        print("Opcion no valida")

if __name__ == "__main__":
    main()
